import sys
import os
import re


def main(path: str):
    if not os.path.isfile(path=path):
        print('Ошибка. Такого файла не существует')
        sys.exit(1)


def find_mail(j: str):
    global x
    if re.findall(mail_pattern, j):
        x += 1


def find_sent(k: str):
    global sent_successfully
    if re.findall(status_sent, k):
        sent_successfully += 1


# def find_sent_with_error(l: str):
#     global sent_with_errors
#     if re.findall(status_not_sent, l):
#         sent_with_errors += 1


if __name__ == '__main__':
    x = 0
    sent_successfully = 0
    status_sent = 'status=sent'
    mail_pattern: str = r'from=<[-_.0-9a-z]+@[-_.0-9a-z]+>'
    with open('maillog.txt', 'r') as log:
        log1 = list(map(str, log.readlines()))
        for i in log1:
            find_mail(i)
            find_sent(i)
            # find_sent_with_error(i)
        quantity_str = len(log1)
        sent_with_errors = int(quantity_str) - int(sent_successfully)

    print(x)
    print(f'Успешно отправленно {sent_successfully}')
    print(f'Отправленно с ошибками {sent_with_errors}')
    # print(log1)
    if len(sys.argv) == 1:
        print("Ошибка. Вы не передали файл логов")
        sys.exit(1)
    main(sys.argv[1])
